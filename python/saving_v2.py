import tensorflow as tf
import numpy as np
import os

def create_model():
    X = tf.keras.Input(shape=(10,), name='input')
    h = tf.keras.layers.Dense(10, kernel_initializer=tf.constant_initializer(1), bias_initializer=tf.constant_initializer(1))(X)
    y = tf.keras.layers.Dense(10, kernel_initializer=tf.constant_initializer(1), bias_initializer=tf.constant_initializer(1), name='output')(h)
    model = tf.keras.models.Model(inputs=[X], outputs=[y])
    return model

def run_model(model: tf.keras.Model):
    output = model.predict(np.array([[1,2,3,4,5,6,7,8,9,10]]))
    print(output)

def save_model_v1(X, y, sess=None):
    '''
    Save a TensorFlow model to a SavedModel file in TensorFlow 1.x
    '''
    if sess is None:
        sess = tf.get_default_session()
    builder = tf.saved_model.builder.SavedModelBuilder('./data/save_model_v1')
    builder.add_meta_graph_and_variables(
        sess,
        [tf.saved_model.tag_constants.SERVING],
        signature_def_map={
            tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: tf.saved_model.predict_signature_def(
                inputs={'X': X},
                outputs={'y': y}
            )
        },
        strip_default_attrs=True)
    builder.save()

def save_model_v2(model):
    '''
    Save a TensorFlow model to a SavedModel file in TensorFlow 2.0
    '''
    tf.saved_model.save(model, './data/save_model_v2')
    model.save('./data/save_model_v2.h5')

def save_model(model: tf.keras.Model):
    if tf.version.VERSION[0] == '1':
        save_model_v1(model.input[0], model.output[0])
    elif tf.version.VERSION[0] == '2':
        save_model_v2(model)

def main():
    print('TensorFlow version: {}'.format(tf.version.VERSION))
    model = create_model()
    if tf.version.VERSION[0] == '1':
        print('Testing TensorFlow 1.x code')
        with tf.Session() as sess:
            run_model(model)
            save_model(model)
        print('After saving model, the variables are:')
        print('Local variables: {}'.format(tf.local_variables()))
        print('Global variables: {}'.format(tf.global_variables()))
    else:
        print('Testing TensorFlow 2.0 code')
        run_model(model)
        save_model(model)

if __name__ == "__main__":
    main()