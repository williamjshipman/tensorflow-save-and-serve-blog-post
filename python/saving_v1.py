import tensorflow as tf
import numpy as np
import os

# Generic functions for creating an running the demo model. No training is done, that's not the point of this.
def create_model():
    X = tf.placeholder(tf.float32, shape=(None, 10), name='input')
    h = tf.layers.Dense(10, kernel_initializer=tf.constant_initializer(1), bias_initializer=tf.constant_initializer(1))(X)
    y = tf.layers.Dense(10, kernel_initializer=tf.constant_initializer(1), bias_initializer=tf.constant_initializer(1), name='output')(h)
    return X, y

def run_model(X, y, sess):
    sess.run(tf.global_variables_initializer())
    output = sess.run([y], feed_dict={X: [[1,2,3,4,5,6,7,8,9,10]]})
    print(output)

# use_tf_saved_model and load_tf_saved_model show how to use the tf.saved_model module in TensorFlow 1.13.

def use_tf_saved_model(X, y, sess):
    builder = tf.saved_model.builder.SavedModelBuilder('./data/save_model')
    builder.add_meta_graph_and_variables(
        sess,
        [tf.saved_model.tag_constants.SERVING],
        signature_def_map={
            tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: tf.saved_model.predict_signature_def(
                inputs={'X': X},
                outputs={'y': y}
            )
        },
        strip_default_attrs=True)
    builder.save()

def load_tf_saved_model(sess, in_name, out_name):
    tf.saved_model.load(sess, [tf.saved_model.tag_constants.SERVING], './data/save_model')
    graph = tf.get_default_graph()
    X = graph.get_tensor_by_name(in_name)
    y = graph.get_tensor_by_name(out_name)
    return X, y

# save_graph_def and load_graph_def show what I think is supposed to work for saving and loading only the GraphDef (not the weights). It doesn't work though.

def save_graph_def(X, y, sess):
    with tf.io.gfile.GFile('./data/tf_io_gfile.pb', mode='wb') as gfile:
        graph = X.graph
        gfile.write(graph.as_graph_def().SerializeToString())

def load_graph_def(sess, in_name, out_name):
    with tf.io.gfile.GFile('./data/tf_io_gfile.pb', mode='rb') as gfile:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(gfile.read())
        sess.graph.as_default()
        tf.import_graph_def(graph_def, name='')
    X = tf.get_default_graph().get_tensor_by_name(in_name)
    y = tf.get_default_graph().get_tensor_by_name(out_name)
    tf.add_to_collection(tf.GraphKeys.GLOBAL_VARIABLES, y)
    return X, y

# save_weights and load_weights use the tf.train.Saver class for saving and loading both the GraphDef and the weights. Too bad this is deprecated in TF 2.0.

def save_weights(sess):
    saver = tf.train.Saver()
    saver.save(sess, './data/tf_io.ckpt')

def load_weights(sess, in_name, out_name):
    tf.train.import_meta_graph('./data/tf_io.ckpt.meta')
    saver = tf.train.Saver()
    saver.restore(sess, './data/tf_io.ckpt')
    X = tf.get_default_graph().get_tensor_by_name(in_name)
    y = tf.get_default_graph().get_tensor_by_name(out_name)
    return X, y

# The main function - uncomment the pairs of function calls for whichever of the above methods you want to tes.

def main():
    X, y = create_model()
    with tf.Session() as sess:
        run_model(X, y, sess)
        # use_tf_saved_model(X, y, sess)
        # save_graph_def(X, y, sess)
        save_weights(sess)
    
    print('After saving model, the variables are:')
    print('Local variables: {}'.format(tf.local_variables()))
    print('Global variables: {}'.format(tf.global_variables()))

    in_name = X.name
    out_name = y.name
    del X, y
    tf.reset_default_graph()

    with tf.Session() as sess:
        print()
        print('Before loading model, the variables are:')
        print('Local variables: {}'.format(tf.local_variables()))
        print('Global variables: {}'.format(tf.global_variables()))
        # X, y = load_tf_saved_model(sess, in_name, out_name)
        # X, y = load_graph_def(sess, in_name, out_name)
        X, y = load_weights(sess, in_name, out_name)
        print()
        print('After loading model, the variables are:')
        print('Local variables: {}'.format(tf.local_variables()))
        print('Global variables: {}'.format(tf.global_variables()))
        run_model(X, y, sess)

if __name__ == "__main__":
    main()