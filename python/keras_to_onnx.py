import tensorflow as tf
import keras2onnx as k2o
import onnx

if __name__ == "__main__":
    model = tf.keras.models.load_model('./data/save_model_v2.h5')
    onnx_model = k2o.convert_keras(model, name=model.name, target_opset=7)
    onnx.save_model(onnx_model, './data/save_model_v2.onnx')